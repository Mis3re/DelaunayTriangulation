Petit soft Java à lancer directement via éclipse qui effectue une triangulation de Delaunay d'un nuage de points généré aléatoirement.
L'algorithme utilisé pour réaliser la triangulation a été inspiré d'un document rédigé par Pierre Audibert : http://www.pierreaudibert.fr/ens/11-Triangulation.pdf
L'algorithme tourne en temps quadratique, ce qui n'est pas optimal mais c'est déjà un bon début.

Réalisé dans le cadre du M2 Informatique Fondamental, Aix-Marseille Université.
Auteur : Pierre-Antoine Martrice
