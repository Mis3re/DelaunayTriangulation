package delaunayTriangulation.view.running.view;

import javafx.scene.layout.BorderPane;

public class GraphePanel extends BorderPane {
	public GraphePanel() {
		//Layout
        this.setStyle("-fx-alignment: center; -fx-background-color: white;");
	}

	public void clearCirclesAndLines() {
		this.getChildren().clear();
	}
}
