package delaunayTriangulation.view.running.view;

import delaunayTriangulation.view.running.managers.Manager;

public class Main {
	private Manager manager;
	private RootLayout rootLayout;
	
	public Main() {
		this.rootLayout = new RootLayout(this);
		this.manager = new Manager(this);
	}

	public RootLayout getRootLayout() {
		return rootLayout;
	}
	public Manager getManagerRun() {
		return manager;
	}
}


