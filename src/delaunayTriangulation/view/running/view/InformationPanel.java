package delaunayTriangulation.view.running.view;

import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

public class InformationPanel extends GridPane{
	private Label nbOfVertexLabel;
	private Label xMinCoordinateBoundLabel;
	private Label xMaxCoordinateBoundLabel;
	private Label yMinCoordinateBoundLabel;
	private Label yMaxCoordinateBoundLabel;

	private TextField nbOfVertex;
	private TextField xMinCoordinateBound;
	private TextField xMaxCoordinateBound;
	private TextField yMinCoordinateBound;
	private TextField yMaxCoordinateBound;
	
	private Button generate;
	
	private Label timeExecutionLabel;
	private Label timeExecution;
	
	public InformationPanel(Main mainRun) {
		nbOfVertexLabel = new Label("Number of vertex : ");
		xMinCoordinateBoundLabel = new Label("Min bound of the abscissa : ");
		xMaxCoordinateBoundLabel = new Label("Max bound of the abscissa : ");
		yMinCoordinateBoundLabel = new Label("Min bound of the ordinate : ");
		yMaxCoordinateBoundLabel = new Label("Max bound of the ordinate : ");

		nbOfVertex = new TextField();
		xMinCoordinateBound = new TextField();
		xMaxCoordinateBound = new TextField();
		yMinCoordinateBound = new TextField();
		yMaxCoordinateBound = new TextField();
		
		generate = new Button("Generate !");
		
		timeExecutionLabel = new Label("Time execution (ms) : ");
		timeExecution = new Label("null");
		
		//Filter
		nbOfVertex.addEventFilter(KeyEvent.KEY_TYPED, numeric_Validation());
		xMinCoordinateBound.addEventFilter(KeyEvent.KEY_TYPED, numeric_Validation());
		xMaxCoordinateBound.addEventFilter(KeyEvent.KEY_TYPED, numeric_Validation());
		nbOfVertex.addEventFilter(KeyEvent.KEY_TYPED, numeric_Validation());
		yMinCoordinateBound.addEventFilter(KeyEvent.KEY_TYPED, numeric_Validation());
		
		this.add(nbOfVertexLabel, 0, 0);
		this.add(xMinCoordinateBoundLabel, 0, 1);
		this.add(xMaxCoordinateBoundLabel, 0, 2);
		this.add(yMinCoordinateBoundLabel, 0, 3);
		this.add(yMaxCoordinateBoundLabel, 0, 4);
		
		this.add(nbOfVertex, 1, 0);
		this.add(xMinCoordinateBound, 1, 1);
		this.add(xMaxCoordinateBound, 1, 2);
		this.add(yMinCoordinateBound, 1, 3);
		this.add(yMaxCoordinateBound, 1, 4);
		
		this.add(generate, 0, 6);
		generate.setOnAction(actionEvent ->  mainRun.getManagerRun().clickOnGenerate());
		
		this.add(timeExecutionLabel, 0, 8);
		this.add(timeExecution, 1, 8);
		
		//Layout
		this.setStyle("-fx-background-color: #CECECE;");
		this.setMinWidth(250);
		this.setPadding(new Insets(7));
		this.setHgap(16);
		this.setVgap(24);
//		this.setGridLinesVisible(true);
		
		ColumnConstraints columnLabels = new ColumnConstraints(155);
		ColumnConstraints columnValues = new ColumnConstraints(30);
		this.getColumnConstraints().addAll(columnLabels, columnValues); 

		nbOfVertexLabel.setWrapText(true);
		xMinCoordinateBoundLabel.setWrapText(true);
		xMaxCoordinateBoundLabel.setWrapText(true);
		yMinCoordinateBoundLabel.setWrapText(true);
		yMaxCoordinateBoundLabel.setWrapText(true);

		nbOfVertex.setMinWidth(60);
		xMinCoordinateBound.setMinWidth(60);
		xMaxCoordinateBound.setMinWidth(60);
		yMinCoordinateBound.setMinWidth(60);
		yMaxCoordinateBound.setMinWidth(60);
		
		nbOfVertex.setAlignment(Pos.CENTER_LEFT);
		xMinCoordinateBound.setAlignment(Pos.CENTER_LEFT);
		xMaxCoordinateBound.setAlignment(Pos.CENTER_LEFT);
		yMinCoordinateBound.setAlignment(Pos.CENTER_LEFT);
		yMaxCoordinateBound.setAlignment(Pos.CENTER_LEFT);

		GridPane.setHgrow(nbOfVertexLabel, Priority.ALWAYS);
		GridPane.setHgrow(xMinCoordinateBoundLabel, Priority.ALWAYS);
		GridPane.setHgrow(xMaxCoordinateBoundLabel, Priority.ALWAYS);
		GridPane.setHgrow(yMinCoordinateBoundLabel, Priority.ALWAYS);
		GridPane.setHgrow(yMaxCoordinateBoundLabel, Priority.ALWAYS);

		GridPane.setValignment(nbOfVertexLabel, VPos.CENTER);
		GridPane.setHalignment(nbOfVertexLabel, HPos.LEFT);
		GridPane.setValignment(xMinCoordinateBoundLabel, VPos.CENTER);
		GridPane.setHalignment(xMinCoordinateBoundLabel, HPos.LEFT);
		GridPane.setValignment(xMaxCoordinateBoundLabel, VPos.CENTER);
		GridPane.setHalignment(xMaxCoordinateBoundLabel, HPos.LEFT);
		GridPane.setValignment(yMinCoordinateBoundLabel, VPos.CENTER);
		GridPane.setHalignment(yMinCoordinateBoundLabel, HPos.LEFT);
		GridPane.setValignment(yMaxCoordinateBoundLabel, VPos.CENTER);
		GridPane.setHalignment(yMaxCoordinateBoundLabel, HPos.LEFT);
		
		generate.setStyle("-fx-font: 16 arial;");
		generate.setMinWidth(230);
		generate.setMinHeight(60);
		
		timeExecution.setMinWidth(120);
		timeExecution.setMinHeight(30);
	}
	
	public EventHandler<KeyEvent> numeric_Validation() {
	    return new EventHandler<KeyEvent>() {
	        @Override
	        public void handle(KeyEvent e) {
	            TextField txt_TextField = (TextField) e.getSource();                
	            if (txt_TextField.getText().length() >= 7) {                    
	                e.consume();
	            }
	            if (!e.getCharacter().matches("[0-9]")) { 
	                e.consume();
	            }
	        }
	    };
	}  
	
	public int getNbOfVertex() {
		if (nbOfVertex.getText().compareTo("") == 0) {
			System.err.println("The number of vertex must to be fill.");
			return -1;
		}
		return Integer.valueOf(nbOfVertex.getText());
	}
	public double getXMinCoordinateBound() {
		if (xMinCoordinateBound.getText().compareTo("") == 0) {
			System.err.println("The min bound of the abscissa must to be fill.");
			return -1;
		}
		return Double.valueOf(xMinCoordinateBound.getText());
	}
	public double getXMaxCoordinateBound() {
		if (xMaxCoordinateBound.getText().compareTo("") == 0) {
			System.err.println("The max bound of the abscissa must to be fill.");
			return -1;
		}
		return Double.valueOf(xMaxCoordinateBound.getText());
	}
	public double getYMinCoordinateBound() {
		if (yMinCoordinateBound.getText().compareTo("") == 0) {
			System.err.println("The min bound of the ordinate must to be fill.");
			return -1;
		}
		return Double.valueOf(yMinCoordinateBound.getText());
	}
	public double getYMaxCoordinateBound() {
		if (yMaxCoordinateBound.getText().compareTo("") == 0) {
			System.err.println("The max bound of the ordinate must to be fill.");
			return -1;
		}
		return Double.valueOf(yMaxCoordinateBound.getText());
	}
	public void setTimeExecution(long time) {
		timeExecution.setText("" + time);
	}
}
