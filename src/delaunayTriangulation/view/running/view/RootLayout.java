package delaunayTriangulation.view.running.view;

import javafx.geometry.Insets;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class RootLayout extends HBox {
	private InformationPanel informationPanel;
	private GraphePanel graphPanel;
	
	public RootLayout(Main mainRun) {
		informationPanel = new InformationPanel(mainRun);
		graphPanel = new GraphePanel();
		this.getChildren().addAll(informationPanel, graphPanel);
		
		//Layout
		this.setFillHeight(true);
		HBox.setHgrow(graphPanel, Priority.ALWAYS);
		HBox.setMargin(graphPanel, new Insets(7));
		informationPanel.setStyle("-fx-background-color: #CECECE;");
	}
	
	public InformationPanel getInformationPanel() {
		return informationPanel;
	}
	public GraphePanel getGraphPanel() {
		return graphPanel;
	}
}
