package delaunayTriangulation.view.running.managers;

import delaunayTriangulation.controller.TriangulationController;
import delaunayTriangulation.model.business.Edge;
import delaunayTriangulation.model.business.Vertex;
import delaunayTriangulation.model.technical.TriangulationTask;
import delaunayTriangulation.view.running.view.GraphePanel;
import delaunayTriangulation.view.running.view.InformationPanel;
import delaunayTriangulation.view.running.view.Main;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

public class Manager {
	private GraphePanel graphPanel;
	private Main main;
	private InformationPanel informationPanel;

	public Manager(Main main) {
		this.graphPanel = main.getRootLayout().getGraphPanel();
		this.main = main;
		this.informationPanel = main.getRootLayout().getInformationPanel();
	}

	public boolean addVertex(Vertex vertex) {
		Circle circle = new Circle();
		circle.setCenterX(vertex.getXCoordinate());
		circle.setCenterY(vertex.getYCoordinate());
		circle.setRadius(5.0f);
		circle.setFill(Color.RED);
		graphPanel.getChildren().add(circle);
		return true;
	}

	public boolean addEdge(Edge edge) {
		Line line = new Line();
		line.setStartX(edge.getAVertex().getXCoordinate());
		line.setStartY(edge.getAVertex().getYCoordinate());
		line.setEndX(edge.getBVertex().getXCoordinate());
		line.setEndY(edge.getBVertex().getYCoordinate());
		graphPanel.getChildren().add(line);
		return true;
	}

	public void clearGraph() {
		graphPanel.clearCirclesAndLines();
	}

	public void clickOnGenerate() {
		long debut = System.currentTimeMillis();

		if (
				informationPanel.getNbOfVertex() == -1 ||
				informationPanel.getXMinCoordinateBound() == -1 ||
				informationPanel.getXMaxCoordinateBound() == -1 ||
				informationPanel.getYMinCoordinateBound() == -1 ||
				informationPanel.getYMaxCoordinateBound() == -1) {
			return;
		}
		main.getManagerRun().clearGraph();
		TriangulationController.getInstance().setNumberOfVertex(informationPanel.getNbOfVertex());
		TriangulationController.getInstance().setXMinCoordinateBound(informationPanel.getXMinCoordinateBound());
		TriangulationController.getInstance().setXMaxCoordinateBound(informationPanel.getXMaxCoordinateBound());
		TriangulationController.getInstance().setYMinCoordinateBound(informationPanel.getYMinCoordinateBound());
		TriangulationController.getInstance().setYMaxCoordinateBound(informationPanel.getYMaxCoordinateBound());
		TriangulationTask.getInstance().init(main);

		long duration = System.currentTimeMillis()-debut;
		informationPanel.setTimeExecution(duration);
	}
}
