package delaunayTriangulation.view;

import javafx.application.Application;
import javafx.stage.Stage;

public class Launcher  extends Application {
	MainWindow mainWindow;

    public static void main(String[] args) {
    	launch(args);
    }

	@Override
	public void start(Stage primaryStage) throws Exception {
		mainWindow = new MainWindow();
		mainWindow.start(primaryStage);		
	}
    
}
