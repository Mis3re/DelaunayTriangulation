package delaunayTriangulation.view;

import delaunayTriangulation.view.running.view.Main;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class MainWindow {
	
	//*** Global parameters ***//
	private String nameApplication = "Delaunay Triangulation";
	private double  minWidthApplication = 200;
	private double  minHeightApplication = 150;
	//***//
	
    private static Stage primaryStage;
	private static Scene scene;

	public void start(Stage primaryStage) throws Exception {
	    MainWindow.primaryStage = primaryStage;
		MainWindow.primaryStage.setTitle(nameApplication);
		MainWindow.primaryStage.setMinWidth(minWidthApplication);
		MainWindow.primaryStage.setMinHeight(minHeightApplication);
		
		Pane root = new BorderPane();
		MainWindow.scene = new Scene(root, 1368, 768);
	    
		Main mainRun = new Main();
		setSceneRoot(mainRun.getRootLayout());
		
		MainWindow.primaryStage.setScene(scene);
		MainWindow.primaryStage.show();
	}
	
    public static Stage getPrimaryStage() {
		return primaryStage;
	}
	
	public static void setSceneRoot(Parent root) {
		MainWindow.scene.setRoot(root);
	}
}
