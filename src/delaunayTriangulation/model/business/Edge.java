package delaunayTriangulation.model.business;

public class Edge {
	private Vertex a;
	private Vertex b;

	public Edge (Vertex a, Vertex b) {
		this.a = a;
		this.b = b;
	}

	public Vertex getAVertex() {
		return a;
	}
	public Vertex getBVertex() {
		return b;
	}
	public String toString() {
		return "edge between " + getAVertex().toString() + " and " + getBVertex().toString();
	}
}
