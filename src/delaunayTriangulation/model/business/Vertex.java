package delaunayTriangulation.model.business;

public class Vertex {
	private String name;
	private double x;
	private double y;
	
	public Vertex (String name, double x, double y) {
		this.name = name;
		this.x = x;
		this.y = y;
	}

	public String getName() {
		return name;
	}
	public double getXCoordinate() {
		return x;
	}
	public double getYCoordinate() {
		return y;
	}
	public String toString() {
		return getName() + "(" + getXCoordinate() + "," + getYCoordinate() + ")";
	}
}