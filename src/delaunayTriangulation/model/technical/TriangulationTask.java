package delaunayTriangulation.model.technical;

import java.util.ArrayList;

import delaunayTriangulation.model.business.Edge;
import delaunayTriangulation.model.business.Vertex;
import delaunayTriangulation.view.running.view.Main;

public class TriangulationTask {
	ArrayList<Vertex> vertexes;
	ArrayList<Edge> edges;
	
	static private TriangulationTask INSTANCE = new TriangulationTask();
	static public TriangulationTask getInstance() {
		return INSTANCE;
	}
	
	public void init(Main mainRun) {
		Algorithms algorithms = new Algorithms();
		VertexGenerator vertexGenerator = new VertexGenerator();
		vertexes = vertexGenerator.generator();
		edges = algorithms.constructiveAlgotithm(vertexes);
		
		for (Vertex vertex : vertexes) {
			mainRun.getManagerRun().addVertex(vertex);
		}
		for (Edge edge : edges) {
			mainRun.getManagerRun().addEdge(edge);
		}
	}
}
