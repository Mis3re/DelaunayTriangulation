package delaunayTriangulation.model.technical;

import java.util.ArrayList;
import java.util.HashMap;

import delaunayTriangulation.model.business.Edge;
import delaunayTriangulation.model.business.Vertex;

public class Algorithms {
	/* http://www.pierreaudibert.fr/ens/11-Triangulation.pdf */
	public ArrayList<Edge> constructiveAlgotithm(ArrayList<Vertex> vertexes) {
		// L'ensemle des ar�tes formant la triangulation.
		ArrayList<Edge> edges = new ArrayList<Edge>();
		// L'ensemble des voisins d'un sommet.
		HashMap<Integer, Vertex> voisins;
		// L'ensemble des sommets g�n�r�s et leur voisins respectifs.
		HashMap<Vertex, HashMap<Integer, Vertex>> voisinsArray = new HashMap<Vertex, HashMap<Integer, Vertex>>();
		// Longueur de l'�ventail pour chaque sommets.
		HashMap<Vertex, Integer> longueur = new HashMap<Vertex, Integer>();
		// Plus proche voisin.
		Vertex ppv;
		//	Voisin suivant de droite.
		Vertex vsd;
		//	Voisin suivant de gauche.
		Vertex vsg;
		Edge edge;
		
		//Pour tout les sommets.
		for (Vertex vertex : vertexes) {
			ppv = plusProcheVoisin(vertex, vertexes);
			voisins = new HashMap<Integer, Vertex>();
			//	Le premier voisin du sommets courant.
			voisins.put(0, ppv);
			voisinsArray.put(vertex, voisins);
			longueur.put(vertex, 1);
			
			int voisinCounter = 0;
			do {
				//Cr�e une ar�te et cherche le voisin suivant � droite.
				edge = new Edge(vertex, voisinsArray.get(vertex).get(voisinCounter));
				vsd = voisinSuivantDroite(edge, vertexes);
				longueur.replace(vertex, longueur.get(vertex) + 1);
				voisinsArray.get(vertex).put(voisinCounter+1, vsd);
				voisinCounter++;
			} while (vsd != ppv && vsd != null && voisinsArray.get(vertex).get(voisinCounter) != null);
			if (vsd == null) {
				do {
					//Si besoin, cr�e une ar�te et cherche le voisin suivant � gauche.
					edge = new Edge(vertex, voisinsArray.get(vertex).get(0));
					vsg = voisinSuivantGauche(edge, vertexes);
					if (vsg != null) {
						for(int k = longueur.get(vertex)-1; k >= 0; k--) {
							voisinsArray.get(vertex).replace(k+1, voisinsArray.get(vertex).get(k));
						}
						voisinsArray.get(vertex).replace(0, vsg);
						longueur.replace(vertex, longueur.get(vertex) + 1);
					}
				} while (vsg != null);
			}
		}
		
		//	Cr�ation de toutes les ar�tes � partir des infomations r�colt�es au dessus.
		for (Vertex vertex : vertexes) {
			for (int i = 0; i < longueur.get(vertex)-1; i++) {
				if (voisinsArray.get(vertex).get(i+1) != null) {
					edges.add(new Edge(vertex, voisinsArray.get(vertex).get(i)));
					edges.add(new Edge(vertex, voisinsArray.get(vertex).get(i + 1)));
					edges.add(new Edge(voisinsArray.get(vertex).get(i), voisinsArray.get(vertex).get(i+1)));
				}
			}
		}
		return edges;
	}
	
	private Vertex plusProcheVoisin(Vertex vertex, ArrayList<Vertex> vertexes) {
		Vertex ppv = null;
		double distance;
		double distanceMin = Double.MAX_VALUE;
		for (Vertex voisin : vertexes) {
			if(vertex != voisin) {
				double vertexX = vertex.getXCoordinate();
				double voisinX = voisin.getXCoordinate();
				double vertexXMinusVoisinX = vertexX - voisinX;
				double vertexY = vertex.getYCoordinate();
				double voisinY = voisin.getYCoordinate();
				double vertexYMinusVoisinY = vertexY - voisinY;
				
				distance = vertexXMinusVoisinX * vertexXMinusVoisinX + vertexYMinusVoisinY * vertexYMinusVoisinY;
				if (distance < distanceMin) {
					distanceMin = distance;
					ppv = voisin;
				}
			}
		}
		return ppv;
	}
	
	private Vertex voisinSuivantDroite(Edge edge, ArrayList<Vertex> vertexes) {
		Vertex voisin = null;
		double v1x;
		double v1y;
		double v2x;
		double v2y;
		double prodscal;
		double longki2;
		double longki;
		double longkj2;
		double longkj;
		double coskij;
		double cosmin = 1;
		double det;
	
		for (Vertex vertex : vertexes) {
			if (edge.getAVertex() != vertex && edge.getBVertex() != vertex) {
				v1x = edge.getBVertex().getXCoordinate() - edge.getAVertex().getXCoordinate();
				v1y = edge.getBVertex().getYCoordinate() - edge.getAVertex().getYCoordinate();
				v2x = vertex.getXCoordinate() - edge.getAVertex().getXCoordinate();
				v2y = vertex.getYCoordinate() - edge.getAVertex().getYCoordinate();
				det = v1x * v2y - v1y * v2x;
				
				// "< 0" parce que � droite. 
				if (det < 0) {
					prodscal = (edge.getAVertex().getXCoordinate() - vertex.getXCoordinate())
							 * (edge.getBVertex().getXCoordinate() - vertex.getXCoordinate())
							 + (edge.getAVertex().getYCoordinate() - vertex.getYCoordinate())
							 * (edge.getBVertex().getYCoordinate() - vertex.getYCoordinate());
					longki2  = (edge.getAVertex().getXCoordinate() - vertex.getXCoordinate())
							 * (edge.getAVertex().getXCoordinate() - vertex.getXCoordinate())
							 + (edge.getAVertex().getYCoordinate() - vertex.getYCoordinate())
							 * (edge.getAVertex().getYCoordinate() - vertex.getYCoordinate());
					longkj2  = (edge.getBVertex().getXCoordinate() - vertex.getXCoordinate())
							 * (edge.getBVertex().getXCoordinate() - vertex.getXCoordinate())
							 + (edge.getBVertex().getYCoordinate() - vertex.getYCoordinate())
							 * (edge.getBVertex().getYCoordinate() - vertex.getYCoordinate());
					longki = Math.sqrt(longki2);
					longkj = Math.sqrt(longkj2);
					coskij = prodscal / (longki * longkj); 
					if (coskij < cosmin) {
						cosmin = coskij;
						voisin = vertex;
					}
				}
			}
		}
		return voisin;
	}
	
	private Vertex voisinSuivantGauche(Edge edge, ArrayList<Vertex> vertexes) {
		Vertex voisin = null;
		double v1x;
		double v1y;
		double v2x;
		double v2y;
		double prodscal;
		double longki2;
		double longki;
		double longkj2;
		double longkj;
		double coskij;
		double cosmin = 1;
		double det;
	
		for (Vertex vertex : vertexes) {
			if (edge.getAVertex() != vertex && edge.getBVertex() != vertex) {
				v1x = edge.getBVertex().getXCoordinate() - edge.getAVertex().getXCoordinate();
				v1y = edge.getBVertex().getYCoordinate() - edge.getAVertex().getYCoordinate();
				v2x = vertex.getXCoordinate() - edge.getAVertex().getXCoordinate();
				v2y = vertex.getYCoordinate() - edge.getAVertex().getYCoordinate();
				det = v1x * v2y - v1y * v2x;
				
				// "> 0" parce que � gauche. 
				if (det > 0) {
					prodscal = (edge.getAVertex().getXCoordinate() - vertex.getXCoordinate())
							 * (edge.getBVertex().getXCoordinate() - vertex.getXCoordinate())
							 + (edge.getAVertex().getYCoordinate() - vertex.getYCoordinate())
							 * (edge.getBVertex().getYCoordinate() - vertex.getYCoordinate());
					longki2  = (edge.getAVertex().getXCoordinate() - vertex.getXCoordinate())
							 * (edge.getAVertex().getXCoordinate() - vertex.getXCoordinate())
							 + (edge.getAVertex().getYCoordinate() - vertex.getYCoordinate())
							 * (edge.getAVertex().getYCoordinate() - vertex.getYCoordinate());
					longkj2  = (edge.getBVertex().getXCoordinate() - vertex.getXCoordinate())
							 * (edge.getBVertex().getXCoordinate() - vertex.getXCoordinate())
							 + (edge.getBVertex().getYCoordinate() - vertex.getYCoordinate())
							 * (edge.getBVertex().getYCoordinate() - vertex.getYCoordinate());
					longki = Math.sqrt(longki2);
					longkj = Math.sqrt(longkj2);
					coskij = prodscal / (longki * longkj); 
					if (coskij < cosmin) {
						cosmin = coskij;
						voisin = vertex;
					}
				}
			}
		}
		return voisin;
	}
}

