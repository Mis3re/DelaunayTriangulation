package delaunayTriangulation.model.technical;

import java.util.ArrayList;

import delaunayTriangulation.controller.TriangulationController;
import delaunayTriangulation.model.business.Vertex;

public class VertexGenerator {
	private int numberOfVertex = TriangulationController.getInstance().getNumberOfVertex();
	private double xMinCoordinateBound = TriangulationController.getInstance().getXMinCoordinateBound();
	private double xMaxCoordinateBound = TriangulationController.getInstance().getXMaxCoordinateBound();
	private double yMinCoordinateBound = TriangulationController.getInstance().getYMinCoordinateBound();
	private double yMaxCoordinateBound = TriangulationController.getInstance().getYMaxCoordinateBound();
	private ArrayList<Vertex> vertexSet;
	
	public ArrayList<Vertex> generator() {
		vertexSet = new ArrayList<Vertex>();
		for (int i = 0; i < numberOfVertex; i++) {
			Vertex newVertex = new Vertex(
					"v" + i,
					xMinCoordinateBound + (Math.random() * (xMaxCoordinateBound - xMinCoordinateBound)),
					yMinCoordinateBound + (Math.random() * (yMaxCoordinateBound - yMinCoordinateBound)));
//			if (isLegalVertex(newVertex)) {
				vertexSet.add(newVertex);
//			}
//			else {
//				i--;
//			}
		}
		return vertexSet;
	}
	
//	private boolean isLegalVertex(Vertex newVertex) {
//		for (Vertex vertex : vertexSet) {
////			if (vertex.getXCoordinate() == newVertex.getXCoordinate() || (vertex.getYCoordinate() == newVertex.getYCoordinate())) {
//			if (Math.abs(vertex.getXCoordinate() - newVertex.getXCoordinate()) < 10
//			 || Math.abs(vertex.getYCoordinate() - newVertex.getYCoordinate()) < 10) {
////				System.err.println("Un point ill�gal a �t� g�n�r�!");
//				return false;
//			}
//		}
//		return true;
//	}
}
