package delaunayTriangulation.controller;

public class TriangulationController {
	static private TriangulationController INSTANCE = new TriangulationController();
	static public TriangulationController getInstance() {
		return INSTANCE;
	}
	
	private int numberOfVertex;
	private double xMinCoordinateBound;
	private double xMaxCoordinateBound;
	private double yMinCoordinateBound;
	private double yMaxCoordinateBound;
	
	public int getNumberOfVertex() {
		return numberOfVertex;
	}
	public double getXMinCoordinateBound() {
		return xMinCoordinateBound;
	}
	public double getXMaxCoordinateBound() {
		return xMaxCoordinateBound;
	}
	public double getYMinCoordinateBound() {
		return yMinCoordinateBound;
	}
	public double getYMaxCoordinateBound() {
		return yMaxCoordinateBound;
	}
	public boolean setNumberOfVertex(int numberOfVertex) {
		if (numberOfVertex < 3) {
			System.err.println("Failed to set the number of vertex : must to be superior or egal to three.");
			return false;
		}
		this.numberOfVertex = numberOfVertex;
		return true;
	}
	public boolean setXMinCoordinateBound(double xMinCoordinateBound) {
		this.xMinCoordinateBound = xMinCoordinateBound;
		return true;
	}
	public boolean setXMaxCoordinateBound(double xMaxCoordinateBound) {
		this.xMaxCoordinateBound = xMaxCoordinateBound;
		return true;
	}
	public boolean setYMinCoordinateBound(double yMinCoordinateBound) {
		this.yMinCoordinateBound = yMinCoordinateBound;
		return true;
	}
	public boolean setYMaxCoordinateBound(double yMaxCoordinateBound) {
		this.yMaxCoordinateBound = yMaxCoordinateBound;
		return true;
	}
}